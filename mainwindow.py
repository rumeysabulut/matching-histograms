
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

import sys
import cv2
from PyQt5 import QtCore, QtGui, QtWidgets
from Image_Histogram import Image_Histogram
from Match_Histogram import Match_Histogram
from matplotlib import pyplot as plt
import numpy as np

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(662, 655)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralWidget)
        self.horizontalLayout.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setSpacing(6)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_3.clicked.connect(self.on_clicked_input)
        self.pushButton_3.setObjectName("pushButton_3")
        self.verticalLayout_4.addWidget(self.pushButton_3)
        self.frame = QtWidgets.QFrame(self.centralWidget)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout_5.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_5.setSpacing(6)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.input_image = QtWidgets.QLabel(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(4)
        sizePolicy.setHeightForWidth(self.input_image.sizePolicy().hasHeightForWidth())
        self.input_image.setSizePolicy(sizePolicy)
        self.input_image.setText("")
        self.input_image.setObjectName("input_image")
        self.verticalLayout_5.addWidget(self.input_image)
        self.inp_f_ch = QtWidgets.QLabel(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.inp_f_ch.sizePolicy().hasHeightForWidth())
        self.inp_f_ch.setSizePolicy(sizePolicy)
        self.inp_f_ch.setText("")
        self.inp_f_ch.setObjectName("inp_f_ch")
        self.verticalLayout_5.addWidget(self.inp_f_ch)
        self.inp_s_ch = QtWidgets.QLabel(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.inp_s_ch.sizePolicy().hasHeightForWidth())
        self.inp_s_ch.setSizePolicy(sizePolicy)
        self.inp_s_ch.setText("")
        self.inp_s_ch.setObjectName("inp_s_ch")
        self.verticalLayout_5.addWidget(self.inp_s_ch)
        self.inp_t_ch = QtWidgets.QLabel(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.inp_t_ch.sizePolicy().hasHeightForWidth())
        self.inp_t_ch.setSizePolicy(sizePolicy)
        self.inp_t_ch.setText("")
        self.inp_t_ch.setObjectName("inp_t_ch")
        self.verticalLayout_5.addWidget(self.inp_t_ch)
        self.verticalLayout_4.addWidget(self.frame)
        self.horizontalLayout.addLayout(self.verticalLayout_4)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setSpacing(6)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_2.clicked.connect(self.on_clicked_target)
        self.pushButton_2.setObjectName("pushButton_2")
        self.verticalLayout_3.addWidget(self.pushButton_2)
        self.frame_2 = QtWidgets.QFrame(self.centralWidget)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.frame_2)
        self.verticalLayout_6.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_6.setSpacing(6)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.target_image = QtWidgets.QLabel(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(4)
        sizePolicy.setHeightForWidth(self.target_image.sizePolicy().hasHeightForWidth())
        self.target_image.setSizePolicy(sizePolicy)
        self.target_image.setText("")
        self.target_image.setObjectName("target_image")
        self.verticalLayout_6.addWidget(self.target_image)
        self.tar_f_ch = QtWidgets.QLabel(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.tar_f_ch.sizePolicy().hasHeightForWidth())
        self.tar_f_ch.setSizePolicy(sizePolicy)
        self.tar_f_ch.setText("")
        self.tar_f_ch.setObjectName("tar_f_ch")
        self.verticalLayout_6.addWidget(self.tar_f_ch)
        self.tar_s_ch = QtWidgets.QLabel(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.tar_s_ch.sizePolicy().hasHeightForWidth())
        self.tar_s_ch.setSizePolicy(sizePolicy)
        self.tar_s_ch.setText("")
        self.tar_s_ch.setObjectName("tar_s_ch")
        self.verticalLayout_6.addWidget(self.tar_s_ch)
        self.tar_t_ch = QtWidgets.QLabel(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.tar_t_ch.sizePolicy().hasHeightForWidth())
        self.tar_t_ch.setSizePolicy(sizePolicy)
        self.tar_t_ch.setText("")
        self.tar_t_ch.setObjectName("tar_t_ch")
        self.verticalLayout_6.addWidget(self.tar_t_ch)
        self.verticalLayout_3.addWidget(self.frame_2)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pushButton = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton.clicked.connect(self.on_clicked_equalized)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout.addWidget(self.pushButton)
        self.frame_3 = QtWidgets.QFrame(self.centralWidget)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.frame_3)
        self.verticalLayout_7.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_7.setSpacing(6)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.result_image = QtWidgets.QLabel(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(4)
        sizePolicy.setHeightForWidth(self.result_image.sizePolicy().hasHeightForWidth())
        self.result_image.setSizePolicy(sizePolicy)
        self.result_image.setText("")
        self.result_image.setObjectName("result_image")
        self.verticalLayout_7.addWidget(self.result_image)
        self.res_f_ch = QtWidgets.QLabel(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.res_f_ch.sizePolicy().hasHeightForWidth())
        self.res_f_ch.setSizePolicy(sizePolicy)
        self.res_f_ch.setText("")
        self.res_f_ch.setObjectName("res_f_ch")
        self.verticalLayout_7.addWidget(self.res_f_ch)
        self.res_s_ch = QtWidgets.QLabel(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.res_s_ch.sizePolicy().hasHeightForWidth())
        self.res_s_ch.setSizePolicy(sizePolicy)
        self.res_s_ch.setText("")
        self.res_s_ch.setObjectName("res_s_ch")
        self.verticalLayout_7.addWidget(self.res_s_ch)
        self.res_t_ch = QtWidgets.QLabel(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.res_t_ch.sizePolicy().hasHeightForWidth())
        self.res_t_ch.setSizePolicy(sizePolicy)
        self.res_t_ch.setText("")
        self.res_t_ch.setObjectName("res_t_ch")
        self.verticalLayout_7.addWidget(self.res_t_ch)
        self.verticalLayout.addWidget(self.frame_3)
        self.horizontalLayout.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 662, 22))
        self.menuBar.setObjectName("menuBar")
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QtWidgets.QToolBar(MainWindow)
        self.mainToolBar.setObjectName("mainToolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_3.setText(_translate("MainWindow", "Input"))
        self.pushButton_2.setText(_translate("MainWindow", "Target"))
        self.pushButton.setText(_translate("MainWindow", "Equalize"))

    def on_clicked_input(self):
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(None,"QFileDialog.getOpenFileName()", "", "Images(*.png *.jpg)")
        
        if (filename):
            pixMap = QtGui.QPixmap(filename)
            self.input_image.setPixmap(pixMap)
            self.img = cv2.imread(filename)
            self.img_obj = Image_Histogram(self.img)
            first_ch_hist, second_ch_hist, third_ch_hist = self.img_obj.histogram()
            
            fig, arr = plt.subplots(3, figsize = (6,3))
            fig.suptitle("Histogram of each channels")

            
            arr[0].bar(np.arange(0, 256),first_ch_hist, color = 'b') 
            arr[1].bar(np.arange(0, 256),second_ch_hist, color = 'g') 
            arr[2].bar(np.arange(0, 256),third_ch_hist, color = 'r') 



            plt.savefig("ch_histograms.jpg")

            pixMap = QtGui.QPixmap("ch_histograms.jpg")
            self.inp_f_ch.setPixmap(pixMap)

            self.inp_first_cdf, self.inp_second_cdf, self.inp_third_cdf = self.img_obj.find_cdf()


    def on_clicked_target(self):
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(None,"QFileDialog.getOpenFileName()", "", "Images(*.png *.jpg)")
        if (filename):
            pixMap = QtGui.QPixmap(filename)
            self.target_image.setPixmap(pixMap)
            self.targetimg = cv2.imread(filename)
            self.img_obj = Image_Histogram(self.targetimg)


            first_ch_hist, second_ch_hist, third_ch_hist = self.img_obj.histogram()
            
            fig, arr = plt.subplots(3, figsize = (6,3))
            fig.suptitle("Histogram of each channels")

            
            arr[0].bar(np.arange(0, 256),first_ch_hist, color = 'b') 
            arr[1].bar(np.arange(0, 256),second_ch_hist, color = 'g') 
            arr[2].bar(np.arange(0, 256),third_ch_hist, color = 'r') 



            plt.savefig("tarch_histograms.jpg")

            pixMap = QtGui.QPixmap("tarch_histograms.jpg")
            self.tar_f_ch.setPixmap(pixMap)

            self.tar_first_cdf, self.tar_second_cdf, self.tar_third_cdf = self.img_obj.find_cdf()


    def on_clicked_equalized(self):
        
        self.match = Match_Histogram(self.inp_first_cdf, self.tar_first_cdf, self.img[:, :, 0])

        matched = self.match.hist_match()

        self.match1 = Match_Histogram(self.inp_second_cdf, self.tar_second_cdf, self.img[:, :, 1])

        matched1 = self.match1.hist_match()

        self.match2 = Match_Histogram(self.inp_third_cdf, self.tar_third_cdf, self.img[:, :, 2])

        matched2 = self.match2.hist_match()

        matched = np.uint8(matched)
        matched1 = np.uint8(matched1)
        matched2 = np.uint8(matched2)

        image = np.dstack((matched, matched1, matched2))
        print(image.shape)
        cv2.imwrite("result_image.jpg", image)


        image = Image_Histogram(image)
        first_ch_hist, second_ch_hist, third_ch_hist = image.histogram()
        
        fig, arr = plt.subplots(3, figsize = (6,3))
        fig.suptitle("Histogram of each channels")

        
        arr[0].bar(np.arange(0, 256),first_ch_hist, color = 'b') 
        arr[1].bar(np.arange(0, 256),second_ch_hist, color = 'g') 
        arr[2].bar(np.arange(0, 256),third_ch_hist, color = 'r') 

        plt.savefig("result_histograms.jpg")

        pixMap = QtGui.QPixmap("result_image.jpg")
        self.result_image.setPixmap(pixMap)

        pixMap2 = QtGui.QPixmap("result_histograms.jpg")
        self.res_f_ch.setPixmap(pixMap2)




class App(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.show()

app = QtWidgets.QApplication(sys.argv)
window = App()
window.show()
sys.exit(app.exec_())

