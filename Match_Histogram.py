
import numpy as np
class Match_Histogram:
	def __init__(self, inp_cdf, tar_cdf, img):
		self.inp_cdf = inp_cdf
		self.tar_cdf = tar_cdf
		self.img = img

	def hist_match(self):	
		r, c = self.img.shape
		matched = np.zeros((r,c))
			
		g_j = 0
		
		for g_i in range(256):
			while(g_j < 255 and self.inp_cdf[g_i] < 1 and self.tar_cdf[g_j] < self.inp_cdf[g_i]):
				g_j = g_j + 1
			matched = matched + (g_j * (self.img == g_i))
			g_j = 0

		return matched
