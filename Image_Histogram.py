import numpy as np
class Image_Histogram:
	def __init__(self, img):
		self.img = img
		self.norm_hist = None

	def histogram(self):
		B, G, R = self.img.shape 
		#B: how many lines of pixels in the image
		#G: how many pixels in one line
		#R: how many bytes in a pixel
		
		hist = np.zeros([256, 1, R])

		for g in range(256):
			
			hist[g, 0, ...] = np.sum(np.sum(self.img == g, 0), 0)
		
		
		first_ch_hist = np.zeros((256, 1))
		second_ch_hist = np.zeros((256, 1))
		third_ch_hist = np.zeros((256, 1))

		first_ch_hist = hist[:, 0, 0]
		second_ch_hist = hist[:, 0, 1]
		third_ch_hist = hist[:, 0, 2]

		#normalized histogram
		total_pix = self.img.size / 3  # Number of pixels in each channel

		self.norm_hist = np.zeros([256, 1, B], dtype=float)
		
		for i in range(hist.shape[0]):
			self.norm_hist[i, 0, 0] = hist[i, 0, 0] / total_pix
			self.norm_hist[i, 0, 1] = hist[i, 0, 1] / total_pix
			self.norm_hist[i, 0, 2] = hist[i, 0, 2] / total_pix
			
		return first_ch_hist, second_ch_hist, third_ch_hist

	def find_cdf(self):
		#find CDF of each channel to match histograms

		first_cdf = np.zeros((256, 1), dtype=float)
		second_cdf = np.zeros((256, 1), dtype=float)
		third_cdf = np.zeros((256, 1), dtype=float)

		first_cdf[0] = self.norm_hist[0, 0, 0]
		second_cdf[0] = self.norm_hist[0, 0, 1]
		third_cdf[0] = self.norm_hist[0, 0, 2]

		for i in range(1, self.norm_hist.shape[0]):
			first_cdf[i] = self.norm_hist[i,:, 0] + first_cdf[i-1]
			second_cdf[i] = self.norm_hist[i,:,1] + second_cdf[i-1]
			third_cdf[i] = self.norm_hist[i,:,2] + third_cdf[i-1]

		return first_cdf, second_cdf, third_cdf