Computer Vision Course First Assignment. It takes an input and a target image. It remaps the first image's histogram to the second image. Also, it displays histograms of different channels of input, target and result images.
Screenshot of the main window should be like this:

![main_window](/uploads/4cf7ef9fc564b16cf406b88c1126a5ce/main_window.png)

The user can choose images from his/her local files by clicking Input and Target buttons. When the user chooses input and target image, he/she can see the images and their histograms of different channels.

![input_target](/uploads/97b5af001e3baec4b3bf0185ad746cc9/input_target.png)

After pressing Equalize button, the input image should change according to the target image's histogram:

![result_image](/uploads/afc95ed309259f92ebf6fdeeaffc4fde/result_image.png)